<?php
session_start();

if(isset($_POST['action'])) {
    if($_POST['action'] === 'login') {
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        // Check if email and password are correct
        if($email === 'johnsmith@gmail.com' && $password === '1234') {
            $_SESSION['email'] = $email;
            header('Location: ./index.php');
            exit();
        } else {
            echo "<p>Invalid email or password.</p>";
        }
    } else if($_POST['action'] === 'logout') {
        // Clear session data and redirect to login page
        session_unset();
        session_destroy();
        header('Location: ./index.php');
        exit();
    }
}
?>