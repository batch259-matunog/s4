<?php

session_start();

// Creating a TaskList class to handle the methods for adding, viewing, editing, and deleting a task.

class TaskList {

  // Add Task Method
  public function add($description){
    $newTask = (object)[
      "description" => $description,
      "isFinished" => false,
    ];

    // if there is no added task yet
    if($_SESSION['tasks'] === null){
      // A global variable "$_SESSION['task']" will be created with an array.
      $_SESSION['tasks'] = array();
    }

    // If there are existing tasks, the $newTask will be added in the "$_SESSION['tasks']" variable.
    array_push($_SESSION['tasks'], $newTask);

  }

  // Update Task Method
  public function update($id, $description, $isFinished){
    // tasks[$id]
    $_SESSION['tasks'][$id]->description = $description;
    $_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
  }

  // Delete Task Method
  public function remove($id){
    //Syntax: array_splice(array, StartDel, length, newArrElement);
    array_splice($_SESSION['tasks'], $id, 1);
  }

  // Clear All Tasks Method
  public function clear(){
    // remove all data associated with the current session.
    session_destroy();
  }

};

// Instantiation of TaskList
$taskList = new TaskList();

// Create an if statement to identify the type of request to be handled by the server.
// Add a Task
if($_POST['action'] === 'add'){
  $taskList->add($_POST['description']);
}
// Update a Task
else if ($_POST['action'] === 'update'){
  $taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}
// Delete a Task
else if ($_POST['action'] === 'delete'){
  $taskList->remove($_POST['id']);
}
// Clear all Tasks
else if ($_POST['action'] === 'clear'){
  $taskList->clear();
}

// it will redirect us to the index file upon sending the request.
header('Location: ./index.php');