<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S04: CLient-Server Communication (Basic To-Do List)</title>
</head>
<body>
  <!-- 
    Session 
      - is a way to store information (in variable) to be used across multiple pages.
   -->

  <!-- Start the session and must be the very first thing to add to your documents before the html tags -->
  <?php session_start(); ?>

  <!-- Form to add tasks -->
  <h3>Add Tasks</h3>
  <form method="POST" action="./server.php">
    <!-- This will identify the action or type of request sent by the user. -->
    <input type="hidden" name="action" value="add">

    Description: <input type="text" name="description" required>

    <button type="submit">Add</button>
  </form>

  <!-- To show all task in the browser using html elements -->
  <h3>Task List</h3>
  <?php if(isset($_SESSION['tasks'])): ?>
    <?php foreach($_SESSION['tasks'] as $id => $task): ?>
      <div>
        <!-- Updating a Task -->
        <form method="POST" action="./server.php" style="display: inline-block">

          <input type="hidden" name="action" value="update">

          <!-- $id will be used for selecting the task to be updated -->
          <input type="hidden" name="id" value="<?php echo $id; ?>">

          <!-- Task Status -->
          <!-- If the tasks is already finished, "checked" attribute will be added, otherwise null. -->
          <input type="checkbox" name="isFinished" <?php echo ($task->isFinished) ? "checked" : null; ?>>

          <input type="text" name="description" value="<?php echo $task->description; ?>">

          <input type="submit" value="Update">
        </form>

        <!-- Form for deleting a task -->
        <form method="POST" action="./server.php" style="display: inline-block">
          <input type="hidden" name="action" value="delete">
          <input type="hidden" name="id" value="<?php echo $id; ?>">
          <input type="submit" value="Delete">
        </form>
      </div>
    <?php endforeach; ?>
  <?php endif; ?>

  <br><br>

  <!-- Add a form to clear all tasks -->
  <form method="POST" action="./server.php">
    <input type="hidden" name="action" value="clear">
    <button type="submit">Clear all tasks</button>
  </form>

</body>
</html>